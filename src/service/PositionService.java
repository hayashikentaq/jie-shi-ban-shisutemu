package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServlet;

import beans.UserPosition;
import dao.PositionDao;

public class PositionService extends HttpServlet {

    public List<UserPosition> getPosition() {

        Connection connection = null;
        try {
            connection = getConnection();

            PositionDao positionDao = new PositionDao();
            List<UserPosition> ret = positionDao.select(connection);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }


}
