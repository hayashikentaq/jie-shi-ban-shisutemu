package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import javax.servlet.http.HttpServlet;

import beans.UserBranch;
import dao.BranchDao;

public class BranchService extends HttpServlet {

	    public List<UserBranch> getBranch() {

	        Connection connection = null;
	        try {
	            connection = getConnection();

	            BranchDao branchDao = new BranchDao();
	            List<UserBranch> ret = branchDao.select(connection);

	            commit(connection);

	            return ret;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	    }


}
