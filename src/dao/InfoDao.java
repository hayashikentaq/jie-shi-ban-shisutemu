package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;

import beans.UserInfo;
import exception.SQLRuntimeException;

public class InfoDao extends HttpServlet {

	public List<UserInfo> select(Connection connection) {

        PreparedStatement ps = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("users.id as id, ");
            sql.append("users.account as account, ");
            sql.append("users.password as password, ");
            sql.append("users.name as name, ");
            sql.append("branches.branch_name as branch_name, ");
            sql.append("departments_positions.position_name as position_name, ");
            sql.append("users.created_at as created_at ");
            sql.append("FROM users ");
            sql.append("INNER JOIN branches ");
            sql.append("ON users.branch = branches.id ");
            sql.append("INNER JOIN departments_positions ");
            sql.append("ON users.department_position = departments_positions.id ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserInfo> ret = toUserInfoList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<UserInfo> toUserInfoList(ResultSet rs)
            throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branchName = rs.getString("branch_name");
                String positionName = rs.getString("position_name");
                Timestamp createdAt = rs.getTimestamp("created_at");

                UserInfo info = new UserInfo();
                info.setId(id);
                info.setAccount(account);
                info.setPassword(password);
                info.setName(name);
                info.setBranch_name(branchName);
                info.setPosition_name(positionName);
                info.setCreated_at(createdAt);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }

    }

	public UserInfo getEdit(Connection connection, int id) {

	    PreparedStatement ps = null;
	    try {
	    	 StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("users.id as id, ");
	            sql.append("users.branch as branch, ");
	            sql.append("users.department_position as department_position, ");
	            sql.append("users.account as account, ");
	            sql.append("users.password as password, ");
	            sql.append("users.name as name, ");
	            sql.append("branches.branch_name as branch_name, ");
	            sql.append("departments_positions.position_name as position_name, ");
	            sql.append("users.created_at as created_at ");
	            sql.append("FROM users ");
	            sql.append("INNER JOIN branches ");
	            sql.append("ON users.branch = branches.id ");
	            sql.append("INNER JOIN departments_positions ");
	            sql.append("ON users.department_position = departments_positions.id ");
	            sql.append("WHERE users.id = ? ");

	        ps = connection.prepareStatement(sql.toString());
	        ps.setInt(1, id);

	        ResultSet rs = ps.executeQuery();
	        List<UserInfo> userList = toUserEditList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<UserInfo> toUserEditList(ResultSet rs)
            throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                int branch = rs.getInt("branch");
                int departmentPosition = rs.getInt("department_position");
                String account = rs.getString("account");
                String password = rs.getString("password");
                String name = rs.getString("name");
                String branchName = rs.getString("branch_name");
                String positionName = rs.getString("position_name");
                Timestamp createdAt = rs.getTimestamp("created_at");

                UserInfo info = new UserInfo();
                info.setId(id);
                info.setBranch(branch);
                info.setDepartment_position(departmentPosition);
                info.setAccount(account);
                info.setPassword(password);
                info.setName(name);
                info.setBranch_name(branchName);
                info.setPosition_name(positionName);
                info.setCreated_at(createdAt);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }

    }

	public UserInfo getAcount(Connection connection, String account) {

	    PreparedStatement ps = null;
	    try {
	    	 StringBuilder sql = new StringBuilder();
	            sql.append("SELECT ");
	            sql.append("users.account as account ");
	            sql.append("FROM users ");
	            sql.append("WHERE users.account = ? ");

	        ps = connection.prepareStatement(sql.toString());
	        ps.setString(1, account);

	        ResultSet rs = ps.executeQuery();
	        List<UserInfo> userList = toUserAccountList(rs);
	        if (userList.isEmpty() == true) {
	            return null;
	        } else if (2 <= userList.size()) {
	            throw new IllegalStateException("2 <= userList.size()");
	        } else {
	            return userList.get(0);
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}

	private List<UserInfo> toUserAccountList(ResultSet rs)
            throws SQLException {

        List<UserInfo> ret = new ArrayList<UserInfo>();
        try {
            while (rs.next()) {
                String account = rs.getString("account");

                UserInfo info = new UserInfo();
                info.setAccount(account);

                ret.add(info);
            }
            return ret;
        } finally {
            close(rs);
        }

    }

}