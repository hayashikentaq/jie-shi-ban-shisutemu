package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranch;
import exception.SQLRuntimeException;

public class BranchDao {

	public List<UserBranch> select(Connection connection) {

        PreparedStatement ps = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("branches.id as id, ");
            sql.append("branches.branch_name as branch_name, ");
            sql.append("branches.created_at as created_at ");
            sql.append("FROM branches ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserBranch> ret = toUserBranchList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<UserBranch> toUserBranchList(ResultSet rs)
            throws SQLException {

        List<UserBranch> ret = new ArrayList<UserBranch>();
        try {
            while (rs.next()) {
                String branchName = rs.getString("branch_name");
                int id = rs.getInt("id");
                Timestamp createdAt = rs.getTimestamp("created_at");

                UserBranch branch = new UserBranch();
                branch.setBranch_name(branchName);
                branch.setId(id);
                branch.setCreated_at(createdAt);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}