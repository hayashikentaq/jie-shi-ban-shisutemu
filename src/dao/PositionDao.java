package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import beans.UserPosition;
import exception.SQLRuntimeException;

@WebServlet("/PositionDao")
public class PositionDao extends HttpServlet {

	public List<UserPosition> select(Connection connection) {

        PreparedStatement ps = null;

        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("departments_positions.id as id, ");
            sql.append("departments_positions.position_name as position_name, ");
            sql.append("departments_positions.created_at as created_at ");
            sql.append("FROM departments_positions ");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserPosition> ret = toUserPositionList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

	private List<UserPosition> toUserPositionList(ResultSet rs)
            throws SQLException {

        List<UserPosition> ret = new ArrayList<UserPosition>();
        try {
            while (rs.next()) {
                String positionName = rs.getString("position_name");
                int id = rs.getInt("id");
                Timestamp createdAt = rs.getTimestamp("created_at");

                UserPosition position = new UserPosition();
                position.setPosition_name(positionName);
                position.setId(id);
                position.setCreated_at(createdAt);

                ret.add(position);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}