package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import beans.UserBranch;
import beans.UserInfo;
import beans.UserPosition;
import service.BranchService;
import service.InfoService;
import service.PositionService;
import service.UserService;


@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<UserBranch> branch = new BranchService().getBranch();
    	request.setAttribute("branch", branch);

    	List<UserPosition> position = new PositionService().getPosition();
    	request.setAttribute("position", position);

    	request.getRequestDispatcher("/signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();


        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setAccount(request.getParameter("account"));
            user.setPassword1(request.getParameter("password1"));
            user.setPassword2(request.getParameter("password2"));
            user.setName(request.getParameter("name"));
            user.setBranch(Integer.parseInt(request.getParameter("branch")));
            user.setDepartment_position(Integer.parseInt(request.getParameter("department_position")));

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String account = request.getParameter("account");
        String password1 = request.getParameter("password1");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");

        UserInfo edit = new InfoService().getAccount(account);

        if (!account.matches("^[a-zA-Z0-9]{6,20}$")) {
            messages.add("ログインIDは半角英数字6文字以上20文字以下で入力してください");
        }
        if (edit != null ) {
            messages.add("そのアカウントは存在しています");
        }
        if (!password1.matches("^[a-zA-Z0-9 -~]{6,20}$")) {
            messages.add("パスワードは記号を含む半角文字6文字以上20文字以下で入力してください");
        } else if (!password2.equals(password1)) {
        	messages.add("パスワードが一致していません");
        }
        if (name.length() > 10) {
        	messages.add("ユーザー名は10文字以下で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}