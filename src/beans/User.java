package beans;

import java.io.Serializable;
import java.util.Date;

public class User implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    private String account;
    public String getAccount() {
    	return account;
    }
    public void setAccount(String account) {
    	this.account = account;
    }

    private String password1;
    public String getPassword1() {
    	return password1;
    }
    public void setPassword1(String password1) {
    	this.password1 = password1;
    }

    private String password2;
    public String getPassword2() {
    	return password2;
    }
    public void setPassword2(String password2) {
    	this.password2 = password2;
    }

    private String name;
    public String getName() {
    	return name;
    }
    public void setName(String name) {
    	this.name = name;
    }

    private int branch;
    public int getBranch() {
    	return branch;
    }
    public void setBranch(int branch) {
    	this.branch = branch;
    }

    private int department_position;
    public int getDepartment_position() {
    	return department_position;
    }
    public void setDepartment_position(int department_position) {
    	this.department_position = department_position;
    }

    private Date created_at;
    public Date getCreated_at() {
    	return created_at;
    }
    public void setCreated_at(Date created_at) {
    	this.created_at = created_at;
    }

    private Date updated_at;
    public Date getUpdated_at() {
    	return updated_at;
    }
    public void setUpdated_at(Date updated_at) {
    	this.updated_at = updated_at;
    }

}