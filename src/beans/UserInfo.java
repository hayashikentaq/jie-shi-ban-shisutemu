package beans;

import java.util.Date;

import javax.servlet.http.HttpServlet;

public class UserInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;

    private int id;
    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    private int branch;
    public int getBranch() {
    	return branch;
    }
    public void setBranch(int branch) {
    	this.branch = branch;
    }

    private int department_position;
    public int getDepartment_position() {
    	return department_position;
    }
    public void setDepartment_position(int department_position) {
    	this.department_position = department_position;
    }

    private String account;
    public String getAccount() {
    	return account;
    }
    public void setAccount(String account) {
    	this.account = account;
    }

    private String password;
    public String getPassword() {
    	return password;
    }
    public void setPassword(String password) {
    	this.password = password;
    }

    private String name;
    public String getName() {
    	return name;
    }
    public void setName(String name) {
    	this.name = name;
    }

    private String branch_name;
    public String getBranch_name() {
    	return branch_name;
    }
    public void setBranch_name(String branch_name) {
    	this.branch_name = branch_name;
    }

    private String position_name;
    public String getPosition_name() {
    	return position_name;
    }
    public void setPosition_name(String position_name) {
    	this.position_name = position_name;
    }

    private Date created_at;
    public Date getCreated_at() {
    	return created_at;
    }
    public void setCreated_at(Date created_at) {
    	this.created_at = created_at;
    }

    private Date updated_at;
    public Date getUpdated_at() {
    	return updated_at;
    }
    public void setUpdated_at(Date updated_at) {
    	this.updated_at = updated_at;
    }

}