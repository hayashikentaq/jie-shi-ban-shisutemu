package beans;

import java.io.Serializable;
import java.util.Date;

public class UserPosition implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    public int getId() {
    	return id;
    }
    public void setId(int id) {
    	this.id = id;
    }

    private String position_name;
    public String getPosition_name() {
    	return position_name;
    }
    public void setPosition_name(String position_name) {
    	this.position_name = position_name;
    }

    private Date created_at;
    public Date getCreated_at() {
    	return created_at;
    }
    public void setCreated_at(Date created_at) {
    	this.created_at = created_at;
    }

    private Date updated_at;
    public Date getUpdated_at() {
    	return updated_at;
    }
    public void setUpdated_at(Date updated_at) {
    	this.updated_at = updated_at;
    }

}