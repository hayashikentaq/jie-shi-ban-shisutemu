<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>新規登録</title>
    </head>
    <body>
    	<div class="main-contents">
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <form action="signup" method="post">

                <label for="account">ログインID</label> <br />
                <input name="account"id="account" /> <br />

                <label for="password1">パスワード</label ><br />
                <input name="password1" type="password" id="password1" /> <br />

                <label for="password2">パスワード（確認用）</label> <br />
                <input name="password2" type="password" id="password2" /> <br />

                <label for="name">ユーザー名</label> <br />
                <input name="name" id="name" /> <br />

                <label for="branch">支店</label> <br />
                <select name="branch" size="1" id="branch">
               		<c:forEach items="${branch}" var="br">
	                <option value="${br.id}">${br.branch_name}</option>
                    </c:forEach>
                </select><br />

                <label for="department_position">部署・役職</label> <br />
                <select name="department_position" size="1" id="department_position">
                	<c:forEach items="${position}" var="po">
	                <option value="${po.id}">${po.position_name}</option>
                    </c:forEach>
                </select><br />

                <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
            </form>
        </div>
    </body>
</html>